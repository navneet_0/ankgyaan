document.getElementById('unityButton').addEventListener('click', () => {
    calculateUnity();
  });



const chaldeanMapping = {
  1 : ['A' ,'I', 'J','Q','Y'],
  2: ['B', 'K', 'R'],
  3: ['C','G','L','S'],
  4: ['D', 'M', 'T'],
  5: ['E','H','N','X'],
  6: ['U','V','W'],
  7: ['O','Z'],
  8: ['F','P']
};

const heartNumberMapping = {
  'A' : 1,
  'E':5,
  'I':1,
  'O':7,
  'U':6
};

var hasNumber = false;
var hasCharactersOnly = false;
var isAlphaNumeric = true;

function isCharLetter(char) {
  return /^[a-zA-Z]$/i.test(char);
};

function getIntValue(value){
  var intValue = 0;
  
  let numDigit = parseInt(value)
  if(!isNaN(numDigit)){
      intValue = numDigit;
  }else{
      isChar = isCharLetter(value)
      if(isChar){
          let charString = value.toUpperCase();
          for (const [key, value] of Object.entries(chaldeanMapping)) {
              if(value.indexOf(charString)>=0){
                  intValue = key;
              }}
      }

  }
  return parseInt(intValue);

};

function reduceToUnity(number){
  console.log("call reduce with ", number, typeof(number));
  if(parseInt(number) !== NaN){
      if(parseInt(number)<=9){
          return number;
      }else{
          
          let numberString = number.toString();
          let tempNumber = 0;
          for(let k=0;k<numberString.length;k++){            
              tempNumber += parseInt(numberString[k]);
          }
      if(tempNumber == 11 || tempNumber == 22){
        return tempNumber;
      }else{
        return reduceToUnity(tempNumber);
      }       
                
      }
  }else{
      return reduceToUnity(parseInt(number));
  }
};

function getUserInput(){
  const inputFromUser = document.getElementById('inputString').value;
  for(let i=0;i<inputFromUser.length;i++){
      isCharLetter(inputFromUser[i]) ? hasCharactersOnly = true : hasNumber = true;
  };
  console.log("Results char only \ alphaNumeric ", hasCharactersOnly, hasNumber);
return inputFromUser;
};

function calculateUnity(){
  
  var userInput = getUserInput();
  let finalNumber = 0;
  
  userInput = userInput.replace(/\s/g,'');

  for(let i=0;i<userInput.length;i++){
      let digitValue = getIntValue(userInput[i]);
      finalNumber += digitValue;
      
  };

  finalNumber = reduceToUnity(finalNumber);
  if(hasCharactersOnly){
    calculateHeartNumber();
    calculateNameNumber();
    calculateMaterialNumber();
  }else{
    document.getElementById('output').innerHTML = "Unity root for "+userInput+" is : "+finalNumber;
  }  
};

function calculateHeartNumber(){
  var name = getUserInput();
  console.log("Heart number for ",name);
  var totalNumber = 0;
  for (let i=0;i<name.length;i++){
    for (const [key, value] of Object.entries(heartNumberMapping)) {
        let charValue = name[i].toUpperCase();
        console.log(key, value, charValue)
      if(charValue === key){
        console.log(value)
        totalNumber += value;
      }
    }
  };
  if(totalNumber>9){
    totalNumber = reduceToUnity(totalNumber);
  };

  document.getElementById('output').innerHTML = "Heart Number for  "+name+" is : "+totalNumber;
  return
};

function calculateNameNumber(){
  var name = getUserInput();
  var nameNumber = 0;
  for(let i=0;i<name.length;i++){
    nameNumber += getIntValue(name[i])
  };
  if (nameNumber >9){
    nameNumber = reduceToUnity(nameNumber);
  };

  document.getElementById('name_number').innerHTML = "Name number "+nameNumber;
  return
};

function calculateMaterialNumber(){
  var name = getUserInput();
  var materialNumber = 0;
  console.log("Material number calculation")

  for(let i=0;i<name.length;i++){
      let charString = name[i].toUpperCase();
      let x = heartNumberMapping[charString];

      // heartNumberMapping[charString] == undefined ? materialNumber += getIntValue(charString) : null;
      
        if(x == undefined){
            materialNumber =+ getIntValue(charString);          
        }                
  };
  if(materialNumber>9){
      materialNumber = reduceToUnity(materialNumber);
  }
  document.getElementById('material_number').innerHTML = "Material number "+materialNumber;
  return
};

