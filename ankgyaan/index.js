const { app, BrowserWindow } = require('electron');
const path = require('node:path');


const createWindow = () => {
  const win = new BrowserWindow({
    width: 1300,
    height: 800,
    webPreferences: {
        preload: path.join(__dirname, 'preloadScripts.js')
      }
  })

  win.loadFile('index.html')
}

app.whenReady().then(() => {
  createWindow()
})